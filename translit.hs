-- created for work with filename in RU lang
-- usage:
-- echo "sometext" | stack runhaskell translit.hs
 
import System.IO

tr x
  | x == 'а' = "a"
  | x == 'б' = "b"
  | x == 'в' = "v"
  | x == 'г' = "g"
  | x == 'д' = "d"
  | x == 'е' = "e"
  | x == 'ё' = "e"
  | x == 'ж' = "zh"
  | x == 'з' = "z"
  | x == 'и' = "i"
  | x == 'й' = "y"
  | x == 'к' = "k"
  | x == 'л' = "l"
  | x == 'м' = "m"
  | x == 'н' = "n"
  | x == 'о' = "o"
  | x == 'п' = "p"
  | x == 'р' = "r"
  | x == 'с' = "s"
  | x == 'т' = "t"
  | x == 'у' = "u"
  | x == 'ф' = "f"
  | x == 'х' = "kh"
  | x == 'ц' = "ts"
  | x == 'ч' = "ch"
  | x == 'ш' = "sh"
  | x == 'щ' = "shch"
  | x == 'ь' = "0"
  | x == 'ы' = "y"
  | x == 'ъ' = "1"
  | x == 'э' = "y"
  | x == 'ю' = "yu"
  | x == 'я' = "ya"
  | x == ' ' = "_"
  | otherwise = [x]

translit [] = ""
translit (x:xs) = tr x ++ translit xs

main :: IO ()
main = interact translit
